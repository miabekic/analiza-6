﻿//Napravite jednostavnu igru vješala.
//Pojmovi se učitavaju u listu iz datoteke, i u svakoj partiji se odabire nasumični pojam iz liste. 
//Omogućiti svu funkcionalnost koju biste očekivali od takve igre.
//Nije nužno crtati vješala, dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo. 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zad2
{
    public partial class Form1 : Form
    {
        char s;
        int brpokusaja, zastavica;
        char[] pogadanarijec, provjeri;
        string r, rez, konacno;
        List<string> rijeci = new List<string>();
        public Form1()
        {
            InitializeComponent();
        }

        private void pokreniigru_Click(object sender, EventArgs e)
        {
            brpokusaja = 6;
            brpokusaj.Text = brpokusaja.ToString();
            string line;
            using(System.IO.StreamReader reader=new System.IO.StreamReader(@"C:\Users\Mia\Desktop\Analiza 6\rijeci.txt"))
            {
                while((line=reader.ReadLine()) != null)
                {
                    rijeci.Add(line);
                }
            }
            Random odabirrijeci = new Random();
            r = rijeci[odabirrijeci.Next(0, rijeci.Count - 1)];
            pogadanarijec = new char[r.Length];
            for(int i = 0; i < r.Length; i++)
            {
                pogadanarijec[i] = '/';
            }
            rez = new string(pogadanarijec);
            pogadanje.Text = rez;
            provjeri = r.ToCharArray();
            textBox2.Text = "";
        }

        private void rijec_Click(object sender, EventArgs e)
        {

        }

        private void pogadanje_Click(object sender, EventArgs e)
        {

        }

        private void provjerislovo_Click(object sender, EventArgs e)
        {
            if (!char.TryParse(textBox1.Text, out s))
            {
                MessageBox.Show("SLOVO NIJE UNESENO!", "GRESKA!");
            }
            else {
                zastavica = 0;
                for (int i = 0; i < r.Length; i++)
                {
                    if (s == provjeri[i])
                    {
                        zastavica = 1;
                        for (int j = 0; j < r.Length; j++)
                        {
                            if (j == i)
                            {
                                pogadanarijec[i] = s;
                            }
                        }
                        rez = new string(pogadanarijec);
                        pogadanje.Text = rez;
                        provjeri = r.ToCharArray();
                        textBox1.Text = "";
                    }
                }
            }
            if (zastavica == 0)
            {
                brpokusaja--;
                brpokusaj.Text = brpokusaja.ToString();
                textBox1.Text = "";
                if (brpokusaja == 0)
                {
                    MessageBox.Show("IZGUBILI STE!", "KRAJ!");
                    Application.Exit();
                }
            }
        }

        private void pokusaji_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void rijesenje_Click(object sender, EventArgs e)
        {
            konacno = textBox2.Text;
            if (konacno != r)
            {
                MessageBox.Show("TO NIJE TRAZENA RIJEC!\nPOKUSAJTE PONOVO!", "NETOCNO!");
                textBox2.Text = "";
            }
            else if(konacno == r) {
                MessageBox.Show("POGODILI STE RIJEC!", "VELIKE CESTITKE!");
            }  
        }

        private void brpokusaj_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
