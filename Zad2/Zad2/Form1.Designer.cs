﻿namespace Zad2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rijec = new System.Windows.Forms.Label();
            this.pokreniigru = new System.Windows.Forms.Button();
            this.izlaz = new System.Windows.Forms.Button();
            this.provjerislovo = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.brpokusaj = new System.Windows.Forms.Label();
            this.pokusaji = new System.Windows.Forms.Label();
            this.rijesenje = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.pogadanje = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rijec
            // 
            this.rijec.AutoSize = true;
            this.rijec.Location = new System.Drawing.Point(103, 51);
            this.rijec.Name = "rijec";
            this.rijec.Size = new System.Drawing.Size(46, 17);
            this.rijec.TabIndex = 0;
            this.rijec.Text = "RIJEČ";
            this.rijec.Click += new System.EventHandler(this.rijec_Click);
            // 
            // pokreniigru
            // 
            this.pokreniigru.Location = new System.Drawing.Point(609, 29);
            this.pokreniigru.Name = "pokreniigru";
            this.pokreniigru.Size = new System.Drawing.Size(135, 110);
            this.pokreniigru.TabIndex = 2;
            this.pokreniigru.Text = "POKRENI IGRU!";
            this.pokreniigru.UseVisualStyleBackColor = true;
            this.pokreniigru.Click += new System.EventHandler(this.pokreniigru_Click);
            // 
            // izlaz
            // 
            this.izlaz.Location = new System.Drawing.Point(669, 398);
            this.izlaz.Name = "izlaz";
            this.izlaz.Size = new System.Drawing.Size(75, 23);
            this.izlaz.TabIndex = 3;
            this.izlaz.Text = "IZLAZ";
            this.izlaz.UseVisualStyleBackColor = true;
            this.izlaz.Click += new System.EventHandler(this.izlaz_Click);
            // 
            // provjerislovo
            // 
            this.provjerislovo.Location = new System.Drawing.Point(106, 248);
            this.provjerislovo.Name = "provjerislovo";
            this.provjerislovo.Size = new System.Drawing.Size(105, 60);
            this.provjerislovo.TabIndex = 4;
            this.provjerislovo.Text = "PROVJERI SLOVO!";
            this.provjerislovo.UseVisualStyleBackColor = true;
            this.provjerislovo.Click += new System.EventHandler(this.provjerislovo_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(300, 267);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 5;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // brpokusaj
            // 
            this.brpokusaj.AutoSize = true;
            this.brpokusaj.Location = new System.Drawing.Point(354, 150);
            this.brpokusaj.Name = "brpokusaj";
            this.brpokusaj.Size = new System.Drawing.Size(0, 17);
            this.brpokusaj.TabIndex = 6;
            this.brpokusaj.Click += new System.EventHandler(this.brpokusaj_Click);
            // 
            // pokusaji
            // 
            this.pokusaji.AutoSize = true;
            this.pokusaji.Location = new System.Drawing.Point(103, 150);
            this.pokusaji.Name = "pokusaji";
            this.pokusaji.Size = new System.Drawing.Size(122, 17);
            this.pokusaji.TabIndex = 7;
            this.pokusaji.Text = "BROJ POKUŠAJA";
            this.pokusaji.Click += new System.EventHandler(this.pokusaji_Click);
            // 
            // rijesenje
            // 
            this.rijesenje.Location = new System.Drawing.Point(106, 359);
            this.rijesenje.Name = "rijesenje";
            this.rijesenje.Size = new System.Drawing.Size(105, 70);
            this.rijesenje.TabIndex = 8;
            this.rijesenje.Text = "RIJEŠENJE";
            this.rijesenje.UseVisualStyleBackColor = true;
            this.rijesenje.Click += new System.EventHandler(this.rijesenje_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(300, 383);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 22);
            this.textBox2.TabIndex = 9;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // pogadanje
            // 
            this.pogadanje.AutoSize = true;
            this.pogadanje.Location = new System.Drawing.Point(332, 51);
            this.pogadanje.Name = "pogadanje";
            this.pogadanje.Size = new System.Drawing.Size(0, 17);
            this.pogadanje.TabIndex = 1;
            this.pogadanje.Click += new System.EventHandler(this.pogadanje_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.rijesenje);
            this.Controls.Add(this.pokusaji);
            this.Controls.Add(this.brpokusaj);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.provjerislovo);
            this.Controls.Add(this.izlaz);
            this.Controls.Add(this.pokreniigru);
            this.Controls.Add(this.pogadanje);
            this.Controls.Add(this.rijec);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label rijec;
        private System.Windows.Forms.Button pokreniigru;
        private System.Windows.Forms.Button izlaz;
        private System.Windows.Forms.Button provjerislovo;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label brpokusaj;
        private System.Windows.Forms.Label pokusaji;
        private System.Windows.Forms.Button rijesenje;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label pogadanje;
    }
}

