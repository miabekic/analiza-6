﻿namespace Zad1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.prvibroj = new System.Windows.Forms.Label();
            this.drugibroj = new System.Windows.Forms.Label();
            this.zbrajanje = new System.Windows.Forms.Button();
            this.oduzimanje = new System.Windows.Forms.Button();
            this.mnozenje = new System.Windows.Forms.Button();
            this.djeljenje = new System.Windows.Forms.Button();
            this.sinus = new System.Windows.Forms.Button();
            this.kosinus = new System.Windows.Forms.Button();
            this.logaritmiranje = new System.Windows.Forms.Button();
            this.korjenovanje = new System.Windows.Forms.Button();
            this.kvadriranje = new System.Windows.Forms.Button();
            this.izlaz = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.minus = new System.Windows.Forms.Label();
            this.puta = new System.Windows.Forms.Label();
            this.podjeljeno = new System.Windows.Forms.Label();
            this.sindrugi = new System.Windows.Forms.Label();
            this.sinprvi = new System.Windows.Forms.Label();
            this.cosdrugi = new System.Windows.Forms.Label();
            this.cosprvi = new System.Windows.Forms.Label();
            this.logdrugi = new System.Windows.Forms.Label();
            this.logprvi = new System.Windows.Forms.Label();
            this.sqrtdrugi = new System.Windows.Forms.Label();
            this.sqrtprvi = new System.Windows.Forms.Label();
            this.kvadratprvi = new System.Windows.Forms.Label();
            this.kvadratdrugi = new System.Windows.Forms.Label();
            this.plus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // prvibroj
            // 
            this.prvibroj.AutoSize = true;
            this.prvibroj.Location = new System.Drawing.Point(13, 9);
            this.prvibroj.Name = "prvibroj";
            this.prvibroj.Size = new System.Drawing.Size(60, 17);
            this.prvibroj.TabIndex = 0;
            this.prvibroj.Text = "Prvi broj";
            this.prvibroj.Click += new System.EventHandler(this.prvibroj_Click);
            // 
            // drugibroj
            // 
            this.drugibroj.AutoSize = true;
            this.drugibroj.Location = new System.Drawing.Point(13, 39);
            this.drugibroj.Name = "drugibroj";
            this.drugibroj.Size = new System.Drawing.Size(70, 17);
            this.drugibroj.TabIndex = 1;
            this.drugibroj.Text = "Drugi broj";
            this.drugibroj.Click += new System.EventHandler(this.drugibroj_Click);
            // 
            // zbrajanje
            // 
            this.zbrajanje.Location = new System.Drawing.Point(16, 78);
            this.zbrajanje.Name = "zbrajanje";
            this.zbrajanje.Size = new System.Drawing.Size(75, 23);
            this.zbrajanje.TabIndex = 2;
            this.zbrajanje.Text = "+";
            this.zbrajanje.UseVisualStyleBackColor = true;
            this.zbrajanje.Click += new System.EventHandler(this.zbrajanje_Click);
            // 
            // oduzimanje
            // 
            this.oduzimanje.Location = new System.Drawing.Point(16, 121);
            this.oduzimanje.Name = "oduzimanje";
            this.oduzimanje.Size = new System.Drawing.Size(75, 23);
            this.oduzimanje.TabIndex = 3;
            this.oduzimanje.Text = "-";
            this.oduzimanje.UseVisualStyleBackColor = true;
            this.oduzimanje.Click += new System.EventHandler(this.oduzimanje_Click);
            // 
            // mnozenje
            // 
            this.mnozenje.Location = new System.Drawing.Point(16, 168);
            this.mnozenje.Name = "mnozenje";
            this.mnozenje.Size = new System.Drawing.Size(75, 23);
            this.mnozenje.TabIndex = 4;
            this.mnozenje.Text = "*";
            this.mnozenje.UseVisualStyleBackColor = true;
            this.mnozenje.Click += new System.EventHandler(this.mnozenje_Click);
            // 
            // djeljenje
            // 
            this.djeljenje.Location = new System.Drawing.Point(16, 211);
            this.djeljenje.Name = "djeljenje";
            this.djeljenje.Size = new System.Drawing.Size(75, 23);
            this.djeljenje.TabIndex = 5;
            this.djeljenje.Text = "/";
            this.djeljenje.UseVisualStyleBackColor = true;
            this.djeljenje.Click += new System.EventHandler(this.djeljenje_Click);
            // 
            // sinus
            // 
            this.sinus.Location = new System.Drawing.Point(16, 253);
            this.sinus.Name = "sinus";
            this.sinus.Size = new System.Drawing.Size(75, 23);
            this.sinus.TabIndex = 6;
            this.sinus.Text = "sin";
            this.sinus.UseVisualStyleBackColor = true;
            this.sinus.Click += new System.EventHandler(this.sinus_Click);
            // 
            // kosinus
            // 
            this.kosinus.Location = new System.Drawing.Point(16, 294);
            this.kosinus.Name = "kosinus";
            this.kosinus.Size = new System.Drawing.Size(75, 23);
            this.kosinus.TabIndex = 7;
            this.kosinus.Text = "cos";
            this.kosinus.UseVisualStyleBackColor = true;
            this.kosinus.Click += new System.EventHandler(this.kosinus_Click);
            // 
            // logaritmiranje
            // 
            this.logaritmiranje.Location = new System.Drawing.Point(16, 335);
            this.logaritmiranje.Name = "logaritmiranje";
            this.logaritmiranje.Size = new System.Drawing.Size(75, 36);
            this.logaritmiranje.TabIndex = 8;
            this.logaritmiranje.Text = "log";
            this.logaritmiranje.UseVisualStyleBackColor = true;
            this.logaritmiranje.Click += new System.EventHandler(this.logaritmiranje_Click);
            // 
            // korjenovanje
            // 
            this.korjenovanje.Location = new System.Drawing.Point(16, 377);
            this.korjenovanje.Name = "korjenovanje";
            this.korjenovanje.Size = new System.Drawing.Size(75, 32);
            this.korjenovanje.TabIndex = 9;
            this.korjenovanje.Text = "sqrt";
            this.korjenovanje.UseVisualStyleBackColor = true;
            this.korjenovanje.Click += new System.EventHandler(this.korjenovanje_Click);
            // 
            // kvadriranje
            // 
            this.kvadriranje.Location = new System.Drawing.Point(16, 418);
            this.kvadriranje.Name = "kvadriranje";
            this.kvadriranje.Size = new System.Drawing.Size(75, 23);
            this.kvadriranje.TabIndex = 10;
            this.kvadriranje.Text = "x^2";
            this.kvadriranje.UseVisualStyleBackColor = true;
            this.kvadriranje.Click += new System.EventHandler(this.kvadriranje_Click);
            // 
            // izlaz
            // 
            this.izlaz.Location = new System.Drawing.Point(682, 391);
            this.izlaz.Name = "izlaz";
            this.izlaz.Size = new System.Drawing.Size(85, 47);
            this.izlaz.TabIndex = 11;
            this.izlaz.Text = "Izlaz";
            this.izlaz.UseVisualStyleBackColor = true;
            this.izlaz.Click += new System.EventHandler(this.izlaz_Click_1);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(127, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 12;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(127, 36);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 22);
            this.textBox2.TabIndex = 13;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // minus
            // 
            this.minus.AutoSize = true;
            this.minus.Location = new System.Drawing.Point(159, 124);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(0, 17);
            this.minus.TabIndex = 14;
            // 
            // puta
            // 
            this.puta.AutoSize = true;
            this.puta.Location = new System.Drawing.Point(159, 174);
            this.puta.Name = "puta";
            this.puta.Size = new System.Drawing.Size(0, 17);
            this.puta.TabIndex = 15;
            // 
            // podjeljeno
            // 
            this.podjeljeno.AutoSize = true;
            this.podjeljeno.Location = new System.Drawing.Point(159, 217);
            this.podjeljeno.Name = "podjeljeno";
            this.podjeljeno.Size = new System.Drawing.Size(0, 17);
            this.podjeljeno.TabIndex = 16;
            // 
            // sindrugi
            // 
            this.sindrugi.AutoSize = true;
            this.sindrugi.Location = new System.Drawing.Point(370, 259);
            this.sindrugi.Name = "sindrugi";
            this.sindrugi.Size = new System.Drawing.Size(0, 17);
            this.sindrugi.TabIndex = 17;
            // 
            // sinprvi
            // 
            this.sinprvi.AutoSize = true;
            this.sinprvi.Location = new System.Drawing.Point(159, 259);
            this.sinprvi.Name = "sinprvi";
            this.sinprvi.Size = new System.Drawing.Size(0, 17);
            this.sinprvi.TabIndex = 18;
            // 
            // cosdrugi
            // 
            this.cosdrugi.AutoSize = true;
            this.cosdrugi.Location = new System.Drawing.Point(370, 294);
            this.cosdrugi.Name = "cosdrugi";
            this.cosdrugi.Size = new System.Drawing.Size(0, 17);
            this.cosdrugi.TabIndex = 19;
            // 
            // cosprvi
            // 
            this.cosprvi.AutoSize = true;
            this.cosprvi.Location = new System.Drawing.Point(159, 300);
            this.cosprvi.Name = "cosprvi";
            this.cosprvi.Size = new System.Drawing.Size(0, 17);
            this.cosprvi.TabIndex = 20;
            // 
            // logdrugi
            // 
            this.logdrugi.AutoSize = true;
            this.logdrugi.Location = new System.Drawing.Point(370, 335);
            this.logdrugi.Name = "logdrugi";
            this.logdrugi.Size = new System.Drawing.Size(0, 17);
            this.logdrugi.TabIndex = 21;
            // 
            // logprvi
            // 
            this.logprvi.AutoSize = true;
            this.logprvi.Location = new System.Drawing.Point(159, 341);
            this.logprvi.Name = "logprvi";
            this.logprvi.Size = new System.Drawing.Size(0, 17);
            this.logprvi.TabIndex = 22;
            // 
            // sqrtdrugi
            // 
            this.sqrtdrugi.AutoSize = true;
            this.sqrtdrugi.Location = new System.Drawing.Point(370, 380);
            this.sqrtdrugi.Name = "sqrtdrugi";
            this.sqrtdrugi.Size = new System.Drawing.Size(0, 17);
            this.sqrtdrugi.TabIndex = 23;
            // 
            // sqrtprvi
            // 
            this.sqrtprvi.AutoSize = true;
            this.sqrtprvi.Location = new System.Drawing.Point(159, 383);
            this.sqrtprvi.Name = "sqrtprvi";
            this.sqrtprvi.Size = new System.Drawing.Size(0, 17);
            this.sqrtprvi.TabIndex = 24;
            // 
            // kvadratprvi
            // 
            this.kvadratprvi.AutoSize = true;
            this.kvadratprvi.Location = new System.Drawing.Point(159, 418);
            this.kvadratprvi.Name = "kvadratprvi";
            this.kvadratprvi.Size = new System.Drawing.Size(0, 17);
            this.kvadratprvi.TabIndex = 25;
            // 
            // kvadratdrugi
            // 
            this.kvadratdrugi.AutoSize = true;
            this.kvadratdrugi.Location = new System.Drawing.Point(370, 418);
            this.kvadratdrugi.Name = "kvadratdrugi";
            this.kvadratdrugi.Size = new System.Drawing.Size(0, 17);
            this.kvadratdrugi.TabIndex = 26;
            // 
            // plus
            // 
            this.plus.AutoSize = true;
            this.plus.Location = new System.Drawing.Point(159, 81);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(0, 17);
            this.plus.TabIndex = 27;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.kvadratdrugi);
            this.Controls.Add(this.kvadratprvi);
            this.Controls.Add(this.sqrtprvi);
            this.Controls.Add(this.sqrtdrugi);
            this.Controls.Add(this.logprvi);
            this.Controls.Add(this.logdrugi);
            this.Controls.Add(this.cosprvi);
            this.Controls.Add(this.cosdrugi);
            this.Controls.Add(this.sinprvi);
            this.Controls.Add(this.sindrugi);
            this.Controls.Add(this.podjeljeno);
            this.Controls.Add(this.puta);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.izlaz);
            this.Controls.Add(this.kvadriranje);
            this.Controls.Add(this.korjenovanje);
            this.Controls.Add(this.logaritmiranje);
            this.Controls.Add(this.kosinus);
            this.Controls.Add(this.sinus);
            this.Controls.Add(this.djeljenje);
            this.Controls.Add(this.mnozenje);
            this.Controls.Add(this.oduzimanje);
            this.Controls.Add(this.zbrajanje);
            this.Controls.Add(this.drugibroj);
            this.Controls.Add(this.prvibroj);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label prvibroj;
        private System.Windows.Forms.Label drugibroj;
        private System.Windows.Forms.Button zbrajanje;
        private System.Windows.Forms.Button oduzimanje;
        private System.Windows.Forms.Button mnozenje;
        private System.Windows.Forms.Button djeljenje;
        private System.Windows.Forms.Button sinus;
        private System.Windows.Forms.Button kosinus;
        private System.Windows.Forms.Button logaritmiranje;
        private System.Windows.Forms.Button korjenovanje;
        private System.Windows.Forms.Button kvadriranje;
        private System.Windows.Forms.Button izlaz;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label minus;
        private System.Windows.Forms.Label puta;
        private System.Windows.Forms.Label podjeljeno;
        private System.Windows.Forms.Label sindrugi;
        private System.Windows.Forms.Label sinprvi;
        private System.Windows.Forms.Label cosdrugi;
        private System.Windows.Forms.Label cosprvi;
        private System.Windows.Forms.Label logdrugi;
        private System.Windows.Forms.Label logprvi;
        private System.Windows.Forms.Label sqrtdrugi;
        private System.Windows.Forms.Label sqrtprvi;
        private System.Windows.Forms.Label kvadratprvi;
        private System.Windows.Forms.Label kvadratdrugi;
        private System.Windows.Forms.Label plus;
    }
}

