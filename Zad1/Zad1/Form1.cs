﻿//Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost znanstvenog kalkulatora,
//odnosno implementirati osnovne(+,-,*,/) i barem 5 naprednih(sin, cos, log, sqrt...) operacija.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zad1
{
    public partial class Form1 : Form
    {
        double x, y;
        public Form1()
        {
            InitializeComponent();
        }

        private void prvibroj_Click(object sender, EventArgs e)
        {

        }

        private void drugibroj_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void zbrajanje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x) && !double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Nije unesen nijedan broj!", "Greska!");
                plus.Text = "";
            }
            else if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Pogreska pri unosu prvog broja!", "Greska!");
                plus.Text = "";
            }
            else if(!double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Pogreska pri unosu drugog broja!", "Greska!");
                plus.Text = "";
            }
            else
            {
                plus.Text = (x + y).ToString();
            }
        }

        private void oduzimanje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x) && !double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Nije unesen nijedan broj!", "Greska!");
                minus.Text = "";
            }
            else if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Pogreska pri unosu prvog broja!", "Greska!");
                minus.Text = "";
            }
            else if (!double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Pogreska pri unosu drugog broja!", "Greska!");
                minus.Text = "";
            }
            else
            {
                minus.Text = (x - y).ToString();
            }
        }

        private void mnozenje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x) && !double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Nije unesen nijedan broj!", "Greska!");
                puta.Text = "";
            }
            else if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Pogreska pri unosu prvog broja!", "Greska!");
                puta.Text = "";
            }
            else if (!double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Pogreska pri unosu drugog broja!", "Greska!");
                puta.Text = "";
            }
            else
            {
                puta.Text = (x * y).ToString();
            }
        }

        private void djeljenje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x) && !double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Nije unesen nijedan broj!", "Greska!");
                podjeljeno.Text = "";
            }
            else if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Pogreska pri unosu prvog broja!", "Greska!");
                podjeljeno.Text = "";
            }
            else if (!double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Pogreska pri unosu drugog broja!", "Greska!");
                podjeljeno.Text = "";
            }
            else
            {
                podjeljeno.Text = (x / y).ToString();
            }
        }

        private void sinus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x) && !double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Nije unesen nijedan broj!", "Greska!");
            }
            else if (!double.TryParse(textBox1.Text, out x))
            {
                sinprvi.Text = "";
                sindrugi.Text = Math.Sin(y).ToString();
            }
            else if (!double.TryParse(textBox2.Text, out y))
            {
                sinprvi.Text = Math.Sin(x).ToString();
                sindrugi.Text = "";
            }
            else
            {
                sinprvi.Text = Math.Sin(x).ToString();
                sindrugi.Text = Math.Sin(y).ToString();
            }
        }

        private void kosinus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x) && !double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Nije unesen nijedan broj!", "Greska!");
            }
            else if (!double.TryParse(textBox1.Text, out x))
            {
                cosprvi.Text = "";
                cosdrugi.Text = Math.Cos(y).ToString();
            }
            else if (!double.TryParse(textBox2.Text, out y))
            {
                cosprvi.Text = Math.Cos(x).ToString();
                cosdrugi.Text = "";
            }
            else
            {
                cosprvi.Text = Math.Cos(x).ToString();
                cosdrugi.Text = Math.Cos(y).ToString();
            }
        }

        private void logaritmiranje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x) && !double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Nije unesen nijedan broj!", "Greska!");
            }
            else if (!double.TryParse(textBox1.Text, out x))
            {
                logprvi.Text = "";
                logdrugi.Text = Math.Log10(y).ToString();
            }
            else if (!double.TryParse(textBox2.Text, out y))
            {
                logprvi.Text = Math.Log10(x).ToString();
                logdrugi.Text = "";
            }
            else
            {
                logprvi.Text = Math.Log10(x).ToString();
                logdrugi.Text = Math.Log10(y).ToString();
            }
        }

        private void korjenovanje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x) && !double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Nije unesen nijedan broj!", "Greska!");
            }
            else if (!double.TryParse(textBox1.Text, out x))
            {
                sqrtprvi.Text = "";
                sqrtdrugi.Text = Math.Sqrt(y).ToString();
            }
            else if (!double.TryParse(textBox2.Text, out y))
            {
                sqrtprvi.Text = Math.Sqrt(x).ToString();
                sqrtdrugi.Text = "";
            }
            else
            {
                sqrtprvi.Text = Math.Sqrt(x).ToString();
                sqrtdrugi.Text = Math.Sqrt(y).ToString();
            }
        }

        private void kvadriranje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x) && !double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Nije unesen nijedan broj!", "Greska!");
            }
            else if (!double.TryParse(textBox1.Text, out x))
            {
                kvadratprvi.Text = "";
                kvadratdrugi.Text = (y * y).ToString();
            }
            else if (!double.TryParse(textBox2.Text, out y))
            {
                kvadratprvi.Text = (x * x).ToString();
                kvadratdrugi.Text = "";
            }
            else
            {
                kvadratprvi.Text = (x * x).ToString();
                kvadratdrugi.Text = (y * y).ToString();
            }
        }

        private void izlaz_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}
